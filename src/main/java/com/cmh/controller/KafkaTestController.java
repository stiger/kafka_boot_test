package com.cmh.controller;

import com.cmh.config.constant.KafkaConstant;
import com.cmh.config.swagger.IgnoreLogin;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 定义kafka每5秒进行生产消息
 * kafka生产者向kafka发送消息
 *
 * @author chenPC
 */
@RestController
@Api(value = "KafkaTestController", tags = {"消息队列-测试"})
@RequestMapping("/kafka")
@EnableScheduling
public class KafkaTestController {
    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    /**
     * 生产者发送kafka消息
     *
     * @return
     */
    @IgnoreLogin
    @Scheduled(cron = "0/5 * *  * * ? ")
    @ApiOperation(value = "发送消息", notes = "发送消息")
    @GetMapping("/sendMsg")
    public boolean send() {
        String message = "test1";
        kafkaTemplate.send(KafkaConstant.TOPIC_NAME, message);
        return true;
    }
}
