package com.cmh.config.utils;

import com.alibaba.fastjson.JSON;
import com.cmh.config.constant.Constant;
import com.cmh.domain.bo.systembo.LoginBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 获取当前登录工具类
 *
 * @author: chenPC
 * @create: 2019-11-26
 * @description:
 **/
@Component
public class AccountUtils {
    @Autowired
    private RedisUtil redisUtil;


    /**
     * 获取当前用户Id工具类
     *
     * @return
     */
    public LoginBO getUser() {
        String accountToken = HttpServletUtils.getRequestHeader(Constant.TOKEN_NAME);
        String token = (String) redisUtil.get(accountToken);
        return JSON.parseObject(token, LoginBO.class);
    }
}
