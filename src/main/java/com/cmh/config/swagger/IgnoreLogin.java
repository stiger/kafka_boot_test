package com.cmh.config.swagger;

import java.lang.annotation.*;

/**
 * swagger免token验证注解
 *
 * @author chenPC
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface IgnoreLogin {
    boolean value() default true;
}
