package com.cmh.config.swagger;

import com.cmh.config.constant.Constant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * swagger2配置类，用于配置swagger2分组
 *
 * @author chenPC
 */
@Configuration
@EnableSwagger2
@Profile({"dev", "test", "prod"})
public class SwaggerConfig {


    //=======================================基本配置================================================

    /**
     * 基本配置
     *
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("title")
                .description("https://www.baidu.com/")
                .termsOfServiceUrl("")
                .version("1.0")
                .build();
    }

    //=======================================获取header信息===========================================

    /**
     * header头信息配置
     *
     * @return
     */
    private List<Parameter> getHeader() {
        ParameterBuilder tokenPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<Parameter>();
        tokenPar.name(Constant.TOKEN_NAME)
                .description("密钥")
                .modelRef(new ModelRef("string"))
                .parameterType("header").required(false);
        pars.add(tokenPar.build());
        return pars;
    }

    //=======================================分组配置===================================================

    /**
     * 接口分组--需要可以多配
     *
     * @return
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("kafka--接口")
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(getHeader())
                .apiInfo(apiInfo());
    }
    //=======================================配置结尾================================================


}
