package com.cmh.config.constant;


/**
 * @author: chenPC
 * @create: 2019-11-27
 * @description: 常量
 **/
public interface Constant {

    /**
     * token
     */
    String TOKEN_NAME = "Authorization";

    /**
     * token存储的key
     **/
    String TOKEN_KEY = "token_";


    Integer TOKEN_EXPIRE_DAY = 7;
    /**
     * token过期时间（秒）
     */
    Integer TOKEN_EXPIRE_TIME = 60 * 60 * 24 * TOKEN_EXPIRE_DAY;


}
