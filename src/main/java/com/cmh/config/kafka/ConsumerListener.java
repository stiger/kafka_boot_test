package com.cmh.config.kafka;

import com.cmh.config.constant.KafkaConstant;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * 监听-消费者，有插入消息马上处理
 *
 * @author chenPC
 */
@Component
public class ConsumerListener {

    /**
     * kafka监听消费者
     *
     * @param message
     */
    @KafkaListener(topics = KafkaConstant.TOPIC_NAME)
    public void onMessage(String message) {
        System.out.println(message);
    }

}