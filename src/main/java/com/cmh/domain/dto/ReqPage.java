package com.cmh.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 分页参数
 *
 * @author chenPC
 */
@Data
public class ReqPage {

    @ApiModelProperty(value = "当前分页数", required = true)
    @Min(value = 0)
    @NotNull
    private Integer page;


    @ApiModelProperty(value = "分页大小", required = true)
    @Min(value = 0)
    @NotNull
    private Integer limit;

}
