package com.cmh.domain.dto.testdto;

import lombok.Data;

/**
 * @author chenPC
 */
@Data
public class TestDTO {
    private String testName;
    private String testPassword;
}
