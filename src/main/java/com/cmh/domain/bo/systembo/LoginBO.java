package com.cmh.domain.bo.systembo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 登录返回对象BO
 *
 * @author chenPC
 */
@Data
public class LoginBO {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "学生姓名")
    private String studentName;

    @ApiModelProperty(value = "用户名")
    private String userPhone;

    @ApiModelProperty(value = "token")
    private String token;

    @ApiModelProperty("token过期时间")
    private Long expirationDate;

}
