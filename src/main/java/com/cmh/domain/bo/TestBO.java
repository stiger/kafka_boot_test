package com.cmh.domain.bo;

import lombok.Data;

/**
 * @author chenPC
 */
@Data
public class TestBO {
    private String testName;
    private String testPassword;
}
