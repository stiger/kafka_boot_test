package com.cmh.domain.vo;

import lombok.Data;

/**
 * @author chenPC
 */
@Data
public class TestVO {
    private String testName;
    private String testPassword;
}
