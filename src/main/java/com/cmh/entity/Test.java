package com.cmh.entity;

import lombok.Data;

/**
 * @author chenPC
 */
@Data
public class Test {
    private String testName;
    private String testPassword;
}
